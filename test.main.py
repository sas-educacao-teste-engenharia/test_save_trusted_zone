import main
import json
import base64


with open(f'test_save_trusted_zone/data/data_question.json') as json_file:
    data = json.load(json_file)
    print(len(data))

    for data_send in data[:5]:
        print(
            f'\n################################################## {str(data_send["id"]).zfill(6)} ###################################################################')
        event = json.dumps(data_send)

        message_bytes = event.encode('utf-8')
        base64_bytes = base64.b64encode(message_bytes)

        main.test_save_trusted_zone({'data': base64_bytes}, context=None)

        print('#############################################################################################################################\n')
