import json
import base64
from utils_gcs import gcs_file_exists, gcs_upload_json
from utils_fs import fs_create_document


#! Global variables
bucket_name = "test_raw_zone"
collection_name = "test_trusted_zone"


def test_save_trusted_zone(event, context=None):

    pubsub_message = base64.b64decode(event['data']).decode('utf-8')
    data = json.loads(pubsub_message)

    print(f'Payload: {data}')

    file_name = f"trusted/id_{str(data['id']).zfill(12)}.json"

    status = gcs_file_exists(bucket_name, file_name)

    print(f'Status: {status}')

    if status == False:
        # Save data in trusted zone GCS
        gcs_upload_json(bucket_name, file_name, data)

        # Save data in trusted zone Firestore
        fs_create_document(collection_name, str(data['id']).zfill(12), data)

        return
    return
