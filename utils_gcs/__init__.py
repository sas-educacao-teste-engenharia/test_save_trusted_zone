from google.cloud import storage
import json


# #! Only need this if you're running this code locally.
# from dotenv import load_dotenv, find_dotenv
# load_dotenv(find_dotenv())
# #! Only need this if you're running this code locally.


def gcs_upload_json(bucket_name, file_name, json_data):
    """Upload json file in Google cloud Storage

    Args:
        bucket_name (string)
        file_name (string)
        json_data (dict): Data to save
    """
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    bucket.blob(file_name).upload_from_string(
        json.dumps(json_data).encode('utf-8'), 'text/json')
    print(f'Document uploaded {file_name} in {bucket_name}')
    return


def gcs_file_exists(bucket_name, file_name):
    """[summary]

    Args:
        bucket_name (string)
        file_name (string)

    Returns:
        bool: Test if exists the file.
    """
    client = storage.Client()
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(file_name)
    return blob.exists()
