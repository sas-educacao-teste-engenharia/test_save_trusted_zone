from google.cloud import firestore
from datetime import datetime, timezone

# #! Only need this if you're running this code locally.
# from dotenv import load_dotenv, find_dotenv
# load_dotenv(find_dotenv())
# #! Only need this if you're running this code locally.

db = firestore.Client()


def fs_create_document(name_collection, name_document, data):

    date = datetime.utcnow()
    data['createdAtISO'] = date.isoformat()

    db.collection(
        name_collection).document(
        name_document).set(
            data)
    print(
        f'Creater document {name_document} in {name_collection}, data: {data}')

    return
